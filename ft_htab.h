/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_htab.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/17 15:42:30 by larrive           #+#    #+#             */
/*   Updated: 2014/11/18 18:45:19 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_HTAB_H
# define FT_HTAB_H

# include <stdlib.h>
# include "ft_ht_list.h"

typedef unsigned long long ull_int;

typedef struct	s_htab
{
	t_ht_list	**tab;
	size_t		size;
}				t_htab;

size_t	ft_htab_keyhash(char *key, size_t tab_size);

t_htab	*ft_htab_create(size_t size);
int		ft_htab_insert(char *key, void *value, t_htab *htab);
void	*ft_htab_get(char *key, t_htab *htab);

#endif
