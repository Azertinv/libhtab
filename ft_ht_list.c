/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ht_list.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/17 19:39:52 by larrive           #+#    #+#             */
/*   Updated: 2014/11/18 23:08:16 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ht_list.h"

t_ht_list	*ft_ht_list_new(void *content, char *key)
{
	t_ht_list	*ret;

	if (!(ret = (t_ht_list *)malloc(sizeof(t_ht_list))))
		return (NULL);
	ret->content = content;
	ret->key = strdup(key); // TODO REPLACE WITH FT
	ret->next = NULL;
	return (ret);
}

t_ht_list	*ft_ht_list_pushfront(t_ht_list **list, t_ht_list *elem)
{
	if (!*list)
		*list = elem;
	else
	{
		elem->next = *list;
		*list = elem;
	}
	return (*list);
}

t_ht_list	*ft_ht_list_pushback(t_ht_list **list, t_ht_list *elem)
{
	t_ht_list	*i;

	if (!*list)
		*list = elem;
	else
	{
		i = *list;
		while (i->next)
			i = i->next;
		i->next = elem;
	}
	return (*list);
}

void	ft_ht_list_del(t_ht_list **list, void(*delf)(void *))
{
	t_ht_list	*next;
	t_ht_list	*tmp;

	if (!*list)
		return ;
	next = *list;
	while (next)
	{
		delf(next->content);
		tmp = next;
		next = tmp->next;
		free(tmp);
	}
	*list = NULL;
}

void	ft_lstsortinsert(t_ht_list **list, t_ht_list *elem,
		int (*cmpf)(void *, void *))
{
	t_ht_list	*i;

	if (!*list || cmpf((*list)->content, elem->content) > 0)
	{
		elem->next = *list;
		*list = elem;
	}
	else
	{
		i = *list;
		while (i->next && cmpf(i->next->content, elem->content) <= 0)
			i = i->next;
		elem->next = i->next;
		i->next = elem;
	}
}
