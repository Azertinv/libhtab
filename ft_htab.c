/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/17 14:05:03 by larrive           #+#    #+#             */
/*   Updated: 2015/01/06 23:19:48 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_htab.h"
#include "ft_ht_list.h"

#include <strings.h> // TODO REMOVE

size_t		ft_htab_keyhash(char *key, size_t tab_size)
{
	size_t 		hash;
	size_t		i;

	i = 0;
	hash = tab_size;
	while (key[0] && key[1] && key[2] && key[3])
	{
		hash ^= *(unsigned int*)key + i++;
		hash *= 171717;
		key += 4;
	}
	while (*key) {
		hash ^= *(unsigned char*)key + i;
		hash *= 171717;
		key++;
	}
	return (hash % tab_size);
}

t_htab	*ft_htab_create(size_t size)
{
	t_htab	*htab;

	htab = (t_htab *)malloc(sizeof(t_htab));
	if (!htab)
		return (NULL);
	htab->tab = (t_ht_list **)malloc(sizeof(t_ht_list *) * size);
	if (!htab)
	{
		free(htab);
		return (NULL);
	}
	bzero(htab->tab, sizeof(t_ht_list *) * size);
	htab->size = size;
	return (htab);
}

int		ft_htab_insert(char *key, void *value, t_htab *htab)
{
	t_ht_list	*elem;
	size_t	hash;

	elem = ft_ht_list_new(value, key);
	if (!elem)
		return (-1);
	hash = ft_htab_keyhash(key, htab->size);
	ft_ht_list_pushfront(htab->tab + hash, elem);
	return (0);
}

void	*ft_htab_get(char *key, t_htab *htab)
{
	t_ht_list	*elem;
	size_t	hash;

	hash = ft_htab_keyhash(key, htab->size);
	elem = (htab->tab)[hash];
	while (elem && strcmp(elem->key, key) != 0) //TODO REPLACE WITH FT
		elem = elem->next;
	if (elem)
		return (elem->content);
	else
		return (NULL);
}

void	ft_htab_clear(t_htab *htab, void(* delf)(void *))
{
	size_t	i;

	i = 0;
	while (i < htab->size)
	{
		if (htab->tab[i])
			delf(htab->tab[i]);
		i++;
	}
	bzero(htab->tab, sizeof(t_ht_list *) * htab->size);
}
