# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: larrive <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/07/15 10:51:27 by larrive           #+#    #+#              #
#    Updated: 2014/11/17 19:47:15 by larrive          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libhtab.a
SRC = ft_htab.c ft_ht_list.c
OBJ = $(SRC:.c=.o)
FLAG = -Wall -Wextra -Werror -O3
CC = gcc $(FLAG)

all: $(NAME)

test: all
	$(CC) $(FLAG) main.c $(NAME)

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

%.o: %.c
	$(CC) -o $@ -c $<

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

deploy: all
	cp $(NAME) ~/lib/
	cp $(NAME:.a=.h) ~/include/

clean-deploy:
	rm ~/lib/$(NAME)
	rm ~/include/$(NAME:.a=.h)

re: fclean all
