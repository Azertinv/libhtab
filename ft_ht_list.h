/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ht_list.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/17 19:40:03 by larrive           #+#    #+#             */
/*   Updated: 2014/11/18 18:47:16 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_HT_LIST_H
# define FT_HT_LIST_H

# include <stdlib.h>

typedef struct	s_ht_list
{
	void			*content;
	char			*key;
	struct s_ht_list	*next;
}				t_ht_list;

t_ht_list	*ft_ht_list_new(void *content, char *key);
t_ht_list	*ft_ht_list_pushfront(t_ht_list **list, t_ht_list *elem);
t_ht_list	*ft_ht_list_pushback(t_ht_list **list, t_ht_list *elem);
void	ft_ht_list_del(t_ht_list **list, void(*delf)(void *));

#endif
