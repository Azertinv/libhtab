/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: larrive <larrive@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/17 14:05:03 by larrive           #+#    #+#             */
/*   Updated: 2014/11/18 13:28:20 by larrive          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <strings.h>
#include "ft_htab.h"

#define KEY_BUFF_SIZE 5
#define VALUE_BUFF_SIZE 20

//int		main(void)
int		main(int ac, char **av)
{
	/*
	int		i;
	int		fd;
	char	key_buffer[KEY_BUFF_SIZE];
	char	value_buffer[VALUE_BUFF_SIZE];
	t_htab	*htab;

	if (!(htab = ft_htab_create(1000000)))
		return (-1);
	fd = open("/dev/urandom", O_RDONLY);
	i = 0;
	while (i < 500000 && read(fd, key_buffer, KEY_BUFF_SIZE - 1))
	{
		key_buffer[KEY_BUFF_SIZE - 1] = '\0';
		read(fd, value_buffer, VALUE_BUFF_SIZE - 1);
		value_buffer[VALUE_BUFF_SIZE - 1] = '\0';
		printf("key:%s\t\thash:%lu\n", key_buffer, ft_htab_keyhash(key_buffer, htab->size));
		i++;
	}
	*/
	int		i;
	int		fd;
	char	key_buffer[KEY_BUFF_SIZE];
	char	value_buffer[VALUE_BUFF_SIZE];
	t_htab	*htab;

	if (!(htab = ft_htab_create(10000000)))
		return (-1);
	fd = open("/dev/urandom", O_RDONLY);
	i = 0;
	if (ft_htab_insert(strdup("test"), strdup("patate"), htab))
		return (-1);
	while (i < 1000000 && read(fd, key_buffer, KEY_BUFF_SIZE - 1))
	{
		key_buffer[KEY_BUFF_SIZE - 1] = '\0';
		read(fd, value_buffer, VALUE_BUFF_SIZE - 1);
		value_buffer[VALUE_BUFF_SIZE - 1] = '\0';
		if (ft_htab_insert(strdup(key_buffer), strdup(value_buffer), htab))
			return (-1);
		i++;
	}
	printf("htab created\n");
	i = 0;
	while (++i < ac)
		printf("key:%s value:%s\n", av[i], (char *)ft_htab_get(av[i], htab));
	return (0);
}
